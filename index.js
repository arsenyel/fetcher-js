import { fetch } from 'whatwg-fetch'

/** @module */

/**
 * @private
 * @description Get first level object length
 *
 * @param {Object} object
 * @returns {Number}
 */
const objectLength = object => Object.keys(object).length

/**
 * @private
 * @description Serialize params to URL
 *
 * @param {string} [base='']
 * @param {*} params
 * @returns {String}
 */
const serialize = (base = '', params) => {
  return objectLength(params) === 0
    ? base
    : base +
        '?' +
        Object.keys(params)
          .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
          .join('&')
}

const postSerialize = (base = '', params) => {
  return Object.keys(params)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&')
}

const defaultError = message => Promise.reject(message)
const callbacks = {
  4: defaultError, /* default handle for 4XX error */
  5: defaultError /* default handle for 5XX error */
}

const setCallback = (number, callback) => {
  callbacks[number] = callback
}

/**
 * @private
 * @description It sends a HTTP request. It's normally called by its synonyms
 *
 * @param {string} [url='']
 * @param {Object} [params={}]
 * @param {string} [method='GET']
 * @param {Object} [content={}]
 * @returns {Promise}
 */
const fetcher = (url = '', params = {}, method = 'GET', content = {}) => {
  // format the url entity/:id/something to entity/1/something (this can be false is there some missing params)
  url = urlMaker(url, params)

  content.method = method

  if (objectLength(params) > 0 && method === 'GET' && url !== false) {
    url = serialize(url, params)
  }

  if (objectLength(params) > 0 && method !== 'GET') {
    content.body = JSON.stringify(params)

    if (content.headers == null) {
      content.headers = {}
    }

    if (content.headers['Content-Type'] == null) {
      content.headers['Content-Type'] = 'application/json'
    }

    if (content.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
      content.body = postSerialize(params)
    }

    if (content.headers['Content-Type'] === 'multipart/form-data') {
      // appends blow
      const newBody = new window.FormData()

      for (const name in params) {
        newBody.append(name, params[name])
      }

      content.body = newBody
    }
  }

  doBefore(content)

  if (url !== false) {
    return fetch(url, content)
      .then(r => {
        const use = r.headers.map['content-type'].indexOf('application/json') !== -1 ? 'json' : 'text'
        if (r.status === 200 || r.status === 201) {
          // Happy path, everything is OK
          return r[use]()
        } else if (r.status >= 400 && r.status < 500) {
          return r[use]()
            .then(data => callbacks[r.status] != null ? callbacks[r.status](data) : callbacks[4](data))
        } else if (r.status >= 500 && r.status < 600) {
          return r[use]()
            .then(data => callbacks[r.status] != null ? callbacks[r.status](data) : callbacks[5](data))
        }
        return Promise.reject(new Error('Error'))
      })
      .catch(e => {
        return Promise.reject(e)
      })
  } else {
    return Promise.reject(new Error('Bad params'))
  }
}

/**
 * @description It makes a GET request
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const get = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'GET', optionalContent)

/**
 * @description It makes a POST request
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url and body
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const post = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'POST', optionalContent)

/**
 * @description It makes a PUT request
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url and body
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const put = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'PUT', optionalContent)

/**
 * @description It makes a DELETE request
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url and body
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const del = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'DELETE', optionalContent)

/**
 * @private
 * @description Search and replace params in urls
 * @example
 * // returns /api/entity/1/name/myvalue
 * urlMaker('/api/entity/:id/name/:value', { id: 1, value: 'myvalue' })
 * @param {string} [url=''] Base url
 * @param {Object} params Params to replace
 * @returns {string} New URL
 */
const urlMaker = (url = '', params) => {
  const expression = /:[a-z]+\/?/gi
  let newUrl = url
  const matches = url.match(expression)

  if (matches != null) {
    matches.map(fullItem => {
      const item = fullItem.substring(1, fullItem.length).replace('/', '')
      if (newUrl !== false && params[item] != null) {
        newUrl = newUrl.replace(':' + item, params[item])
      } else {
        newUrl = false
      }
    })
  }

  return newUrl
}

let before = () => {}
const setBefore = callback => {
  before = callback
}
const doBefore = content => {
  before(content)
}

export default {
  get,
  post,
  put,
  del,
  setCallback,
  setBefore
}
